﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitializeDataGridView();
        }

        //Методы.
        //Инициализация DataGridView.
        private void InitializeDataGridView()
        {
            var column1 = new DataGridViewColumn();
            column1.HeaderText = "Название"; //текст в шапке
            column1.Width = 80; //ширина колонки
            column1.Name = "name"; //текстовое имя колонки, его можно использовать вместо обращений по индексу
            column1.Frozen = true; //флаг, что данная колонка всегда отображается на своем месте
            column1.CellTemplate = new DataGridViewTextBoxCell(); //тип колонки

            var column2 = new DataGridViewColumn();
            column2.HeaderText = "Значение";
            column2.Width = 155;
            column2.Name = "value";
            column2.Frozen = true;
            column2.CellTemplate = new DataGridViewTextBoxCell();

            DataGridView.Columns.Add(column1);
            DataGridView.Columns.Add(column2);
        }
        //Получение данных из DataGridView
        private void GetValue(out List<float> vs, out List<string> name)
        {
            List<float> points = new List<float>();
            List<string> list = new List<string>();
            vs = null;
            name = null;

            foreach (DataGridViewRow row in DataGridView.Rows)
            {
                if (row.IsNewRow) continue;
                float val;

                if (row.Cells["value"].Value == null || row.Cells["name"].Value == null)
                {
                    MessageBox.Show("Не все строки заполнены корректно.");
                    return;
                }

                if (!Single.TryParse(row.Cells["value"].Value.ToString(), out val))
                {
                    MessageBox.Show($"Строка {row.Cells["name"].Value}: введите число.");
                    return;
                }

                if (val < 0)
                {
                    MessageBox.Show($"Строка {row.Cells["name"].Value}: значение должно быть больше либо равно нулю.");
                    return;
                }

                points.Add(val);
                list.Add(row.Cells["name"].Value.ToString());
            }

            vs = points;
            name = list;
        }

        private float GetMax(List<float> list)
        {
            float max = list[0];
            for (int i = 1; i < list.Count; i++)
            {
                if (max < list[i])
                    max = list[i];
            }
            return max;
        }
        //Получение высоты, ширины панели и расчёт радиуса.
        private void GetConstVal(out int height, out int width, out float radius)
        {
            height = panel_canvas.Height;
            width = panel_canvas.Width;
            radius = (width > height) ? (float)height : (float)width;

            if (radius % 2 == 0) radius /= 2;
            else radius = (radius - 1) / 2;
        }
        //Получение координат центра панели.
        private void GetOrigin(int height, int width, out PointF pointF)
        {
            PointF point = new PointF();

            point.X = (width % 2 == 0) ? (width / 2) : ((width - 1) / 2);
            point.Y = (height % 2 == 0) ? (height / 2) : ((height - 1) / 2);

            pointF = point;
        }
        //Получение координат для правильного многоугольника.
        private PointF[] GetVertexPolygon(float R, PointF center, int count)
        {
            List<PointF> points = new List<PointF>();
            PointF point = new PointF();
            double pre_angle = 2 * Math.PI / count;//Угол в радианах.
            float angle;

            for (int i = 0; i < count; i++)
            {
                angle = (float)(pre_angle * i) - (float)(Math.PI / 2);
                point.X = center.X + (float)(R * Math.Cos(angle));
                point.Y = center.Y + (float)(R * Math.Sin(angle));
                points.Add(point);
            }

            return points.ToArray();
        }
        //Получение координат для многоугольника, который строится в соответсвии с введёными значениям.
        private PointF[] GetVertexPolygonFill(List<float> coordinate, PointF center, float radius)
        {
            List<PointF> points = new List<PointF>();
            PointF point = new PointF();
            double angle = 2 * Math.PI / coordinate.Count;//Угол в радианах.
            float max = GetMax(coordinate);
            float single_segment = radius / max;

            for (int i = 0; i < coordinate.Count; i++)
            {
                point.X = (float)Math.Cos(angle * i - Math.PI / 2) * single_segment * coordinate[i] + center.X;
                point.Y = (float)Math.Sin(angle * i - Math.PI / 2) * single_segment * coordinate[i] + center.Y;
                points.Add(point);
            }

            return points.ToArray();
        }
        //Рисует сетку из многоугольников и линий.
        private void DrawGrid(Graphics canvas, List<float> cord, float radius, PointF center, Pen pen)
        {
            int len = cord.Count;
            float single_segment = radius / len;
            
            PointF[] points = GetVertexPolygon(radius, center, len);

            for (int i = 0; i < len; i++)
            {
                canvas.DrawPolygon(pen, GetVertexPolygon(single_segment * (i+1), center, len));
                canvas.DrawLine(pen, center, points[i]);
            }
        }
        //Добавляет подписи вершин и шкалу.
        private void DrawSign(Graphics canvas, List<float> cord, List<string> titles, PointF center, float radius)
        {
            string str;
            int j = 0;
            int count = cord.Count;
            float sign = GetMax(cord) / count;
            float single_segment = radius / count;
            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            for (int i = 0; i <= count; i++)
            {
                str = (i * sign).ToString();
                canvas.DrawString(str, drawFont, drawBrush, center.X, center.Y - i * single_segment);
            }

            center.Y -= 7;
            center.X -= 5;
            foreach (PointF item in GetVertexPolygon(radius + 10, center, count))
            {
                canvas.DrawString(titles[j], drawFont, drawBrush, item);
                j++;
            }
        }
        //Рисует лепестковую диаграмму.
        private void DrawRadarChart(List<float> cord, List<string> titles)
        {
            Graphics g = panel_canvas.CreateGraphics();
            SolidBrush brush = new SolidBrush(Color.Aqua);
            Pen pen = new Pen(Color.Black, 1);
            PointF center;
            int h, w;
            float R;

            GetConstVal(out h, out w, out R);
            GetOrigin(h, w, out center);

            R -= 40;
            g.Clear(Color.White);
            g.FillPolygon(brush, GetVertexPolygonFill(cord, center, R));
            DrawGrid(g, cord, R, center, pen);
            DrawSign(g, cord, titles, center, R);
        }

        //Обработчики событий.
        private void button_draw_Click(object sender, EventArgs e)
        {
            GetValue(out List<float> coordinates, out List<string> title_list);

            if (coordinates == null) return;

            if (coordinates.Count < 3)
            {
                MessageBox.Show("Заполните как минимум 3 строки.");
                return;
            }

            DrawRadarChart(coordinates, title_list);
        }
    }
}
